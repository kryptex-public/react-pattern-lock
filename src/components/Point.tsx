import * as React from "react";
import { Point as PointVisual } from "../types";

interface PointProps {
    index           : number;  //index dans le tableau de points (dans l'ordre)
    pointSize       : number;  //taille du point
    pointActiveSize : number;  //taille de la zone active autour du point
    size            : number;
    pop             : boolean;
    selected        : boolean;
    position        : PointVisual;
}

const PointVisual: React.FunctionComponent<PointProps> = ({
    index,
    pointSize,
    pointActiveSize,
    size,
    selected,
    pop,
    position
}) => {
    const percentPerItem = 100 / size;

    return (
        <div
            className={ `react-pattern-lock__point-wrapper${selected ? " selected" : ""}` }
            style={{
                position: 'absolute',
                top: position.y+'px',
                left: position.x+'px',
            }}
            data-index={ index }
        >
            <div
                className="react-pattern-lock__point"
                style={{
                    width  : pointActiveSize,    //largeur du point "actif" (zone de tolérance)
                    height : pointActiveSize     //hauteur du point "actif" (zone de tolérance)
                }}
            >
                <div
                    className={ `react-pattern-lock__point-inner${pop ? " active" : ""}` }
                    style={{
                        minWidth   : pointSize,  //largeur du point affiché
                        minHeight  : pointSize   //largeur du point affiché
                    }}
                />
            </div>
        </div>
    );
};


export default PointVisual;