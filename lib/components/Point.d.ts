import * as React from "react";
import { Point as PointVisual } from "../types";
interface PointProps {
    index: number;
    pointSize: number;
    pointActiveSize: number;
    size: number;
    pop: boolean;
    selected: boolean;
    position: PointVisual;
}
declare const PointVisual: React.FunctionComponent<PointProps>;
export default PointVisual;
