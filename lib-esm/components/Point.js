import * as React from "react";
var PointVisual = function (_a) {
    var index = _a.index, pointSize = _a.pointSize, pointActiveSize = _a.pointActiveSize, size = _a.size, selected = _a.selected, pop = _a.pop, position = _a.position;
    var percentPerItem = 100 / size;
    return (React.createElement("div", { className: "react-pattern-lock__point-wrapper".concat(selected ? " selected" : ""), style: {
            position: 'absolute',
            top: position.y + 'px',
            left: position.x + 'px',
        }, "data-index": index },
        React.createElement("div", { className: "react-pattern-lock__point", style: {
                width: pointActiveSize,
                height: pointActiveSize
            } },
            React.createElement("div", { className: "react-pattern-lock__point-inner".concat(pop ? " active" : ""), style: {
                    minWidth: pointSize,
                    minHeight: pointSize
                } }))));
};
export default PointVisual;
//# sourceMappingURL=Point.js.map